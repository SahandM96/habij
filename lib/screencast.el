(defun minibuffer-setup ()
  (set (make-local-variable 'face-remapping-alist)
       '((default :height 1.5))))

(defun screencast-init ()
  (interactive)
  (add-hook 'minibuffer-setup-hook 'minibuffer-setup)
  (add-to-list 'default-frame-alist
               '(font . "FiraMono-18"))
  (set-frame-font "FiraMono 18" nil t))

(provide 'screencast)
